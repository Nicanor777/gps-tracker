select ride_id, 
ride_date,
ride_type,
skipped_station_id,
  SAFE_DIVIDE(socket_connected_count,datetime_diff(datetime(completed_at),datetime(started_at),second)/2) as gps_pings_per 

from
   (select r._id as ride_id,completed_at,started_at, max(lr.socket_connected_count) as socket_connected_count,
   r.date as ride_date,
   r.type as ride_type,
   skipped as skipped_station_id

    from ride.rides r , unnest(skipped_stations) skipped
    left join unnest(location_report_mobile_location_counts) lr
    left join ride.cities cities on cities._id = r.city
    where cities.name = 'Nairobi'
    and r.type not in ("training")
    and date(r.date,"Africa/Nairobi") = date("2022-05-24")
    group by 1,2,3,5,ride_type,skipped_station_id
    )
